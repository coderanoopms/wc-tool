package main

import (
	"os"
	"os/exec"
	"strings"
	"testing"
)

//func build() error {
//	buildCmd := exec.Command("go", "build", "-o", "ccwc")
//	buildErr := buildCmd.Run()
//	return buildErr
//}

func TestMain(m *testing.M) {
	// Build the binary before running any tests
	buildCmd := exec.Command("go", "build", "-o", "ccwc")
	err := buildCmd.Run()
	if err != nil {
		os.Exit(1)
	}
	binaryPath := "./ccwc" // Set the path to the built binary
	// Run the tests
	code := m.Run()
	// Perform cleanup, like removing the built binary
	os.Remove(binaryPath)
	// Exit with the code from the test run
	os.Exit(code)
}

func TestCCWCNoFileArg(t *testing.T) {
	cmd := exec.Command("./ccwc", "ccwc", "-l") // Adjust for your binary name and path
	output, _ := cmd.CombinedOutput()
	expectedErrorMessage := "Error: No file entered as argument"
	if !strings.Contains(string(output), expectedErrorMessage) {
		t.Errorf("Expected error message '%s', got '%s'", expectedErrorMessage, string(output))
	}
}

func TestCCWCWrongOption(t *testing.T) {
	cmd := exec.Command("./ccwc", "ccwc", "-p") // Adjust for your binary name and path
	output, _ := cmd.CombinedOutput()
	expectedErrorMessage := "Error: unknown shorthand flag: 'p'"
	if !strings.Contains(string(output), expectedErrorMessage) {
		t.Errorf("Expected error message '%s', got '%s'", expectedErrorMessage, string(output))
	}
}

func TestCCWCPrintLinesOption(t *testing.T) {
	cmd := exec.Command("./ccwc", "ccwc", "-l", "test.txt") // Adjust for your binary name and path
	output, _ := cmd.CombinedOutput()
	expectedOutPut := "7145   test.txt\n"
	if !strings.Contains(string(output), expectedOutPut) {
		t.Errorf("Expected error message '%s', got '%s'", expectedOutPut, string(output))
	}
}

func TestCCWCPrintWordsOption(t *testing.T) {
	cmd := exec.Command("./ccwc", "ccwc", "-w", "test.txt") // Adjust for your binary name and path
	output, _ := cmd.CombinedOutput()
	expectedOutPut := "58164   test.txt\n"
	if !strings.Contains(string(output), expectedOutPut) {
		t.Errorf("Expected error message '%s', got '%s'", expectedOutPut, string(output))
	}
}

func TestCCWCPrintBytesOption(t *testing.T) {
	cmd := exec.Command("./ccwc", "ccwc", "-c", "test.txt") // Adjust for your binary name and path
	output, _ := cmd.CombinedOutput()
	expectedOutPut := "342190   test.txt\n"
	if !strings.Contains(string(output), expectedOutPut) {
		t.Errorf("Expected error message '%s', got '%s'", expectedOutPut, string(output))
	}
}
