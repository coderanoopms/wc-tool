package main

import (
	"bufio"
	"fmt"
	"io/fs"
	"log"
	"os"
	"unicode/utf8"

	"github.com/spf13/cobra"
)

func main() {
	var rootCmd = &cobra.Command{
		Use:   "app",
		Short: "App is a CLI application",
		Long:  `App is a CLI application for WC`,
	}

	var cFlag, lFlag, wFlag, mFlag bool // Variable to store the state of the -l flag
	var file *os.File
	var err error

	var ccwcCmd = &cobra.Command{
		Use:   "ccwc [filename]",
		Short: "word count",
		Long:  `Word count`,
		//Args:  cobra.MinimumNArgs(1), // Ensures that at least one argument is provided
		Run: func(cmd *cobra.Command, args []string) {
			var isFileFlag bool
			if isInputFromPipe() {
				file = os.Stdin
				isFileFlag = false
			} else {
				if len(args) == 0 {
					fmt.Println("Error: No file entered as argument")
					return
				}
				filePath := args[0] // Get the filename argument
				file, err = openFile(filePath)
				if err != nil {
					fmt.Println("Error:", err.Error())
					return
				}
				isFileFlag = true
			}

			if cFlag {
				printBytes(file, isFileFlag)
			} else if lFlag {
				printLines(file, isFileFlag)
			} else if wFlag {
				printWords(file, isFileFlag)
			} else if mFlag {
				printCharacters(file, isFileFlag)
			} else {
				printDefault(file)
			}
			defer file.Close()
		},
	}

	ccwcCmd.Flags().BoolVarP(&cFlag, "bytes", "c", false, "Long format flag")
	ccwcCmd.Flags().BoolVarP(&lFlag, "lines", "l", false, "Long format flag")
	ccwcCmd.Flags().BoolVarP(&wFlag, "words", "w", false, "Long format flag")
	ccwcCmd.Flags().BoolVarP(&mFlag, "characters", "m", false, "Long format flag")

	// Add the ccwc command to the root command
	rootCmd.AddCommand(ccwcCmd)

	// Execute the root command
	if err := rootCmd.Execute(); err != nil {
		fmt.Println(err)
		os.Exit(1)
	}
}

func isInputFromPipe() bool {
	fileInfo, _ := os.Stdin.Stat()
	return fileInfo.Mode()&os.ModeCharDevice == 0
}

// openFile
//
//	@Description: Function to open a file
//	@param filePath
//	@return *os.File
//	@return error
func openFile(filePath string) (*os.File, error) {
	filename := filePath

	// Open the file
	file, err := os.Open(filename)
	if err != nil {
		return nil, err
	}
	return file, nil
}

// getFileInfo
//
//	@Description: Function to get fileinfo
//	@param file
//	@return fs.FileInfo
//	@return error
func getFileInfo(file *os.File) (fs.FileInfo, error) {
	// Get file statistics
	fileInfo, err := file.Stat()
	if err != nil {
		fmt.Println("Error getting file info:", err)
		return nil, err
	}
	return fileInfo, nil
}

// printDefault
//
//	@Description: Func to print when the flag is default
//	@param file
func printDefault(file *os.File) {
	fileName := file.Name()
	// Reset the file pointer to the beginning of the file
	_, err := file.Seek(0, 0)
	if err != nil {
		log.Fatal("Error seeking file:", err)
	}
	wordCount := calculateWords(file)
	// Reset the file pointer to the beginning of the file
	_, err = file.Seek(0, 0)
	if err != nil {
		log.Fatal("Error seeking file:", err)
	}
	bytes := calculateBytes(file)
	lineCount := calculateLines(file)

	fmt.Printf("%d  %d  %d  %s\n", lineCount, wordCount, bytes, fileName)
}

// printBytes
//
//	@Description: Function to print bytes
//	@param fileInfo
func printBytes(file *os.File, isFileFlag bool) {
	bytes := calculateBytes(file)
	printOutput(bytes, file.Name(), isFileFlag)
}

// calculateBytes
//
//	@Description: Fun to calculate bytes
//	@param file
//	@return int64
func calculateBytes(file *os.File) int64 {
	fileInfo, err := getFileInfo(file)
	if err != nil {
		fmt.Println("Error:", err)
	}
	bytes := fileInfo.Size()
	return bytes
}

// printLines
//
//	@Description: Function to print lines
//	@param fileInfo
func printLines(file *os.File, isFileFlag bool) {
	lineCount := calculateLines(file)
	printOutput(lineCount, file.Name(), isFileFlag)
}

// calculateLines
//
//	@Description: Function to calculate line
//	@param file
//	@return int
func calculateLines(file *os.File) int {
	scanner := bufio.NewScanner(file)
	lineCount := 0
	for scanner.Scan() {
		lineCount++
	}
	return lineCount
}

// printWords
//
//	@Description: Function to calculate number of words in a file
//	@param file
func printWords(file *os.File, isFileFlag bool) {
	wordCount := calculateWords(file)
	printOutput(wordCount, file.Name(), isFileFlag)
}

func calculateWords(file *os.File) int {
	// Create a scanner to read the file
	scanner := bufio.NewScanner(file)
	// Set the split function for the scanning operation.
	scanner.Split(bufio.ScanWords)
	// Count the words
	wordCount := 0
	for scanner.Scan() {
		wordCount++
	}
	return wordCount
}

// printCharacters
//
//	@Description: Function to calculate number of characters in a file
//	@param file
func printCharacters(file *os.File, isFileFlag bool) {
	// Read the file
	fileName := file.Name()
	data, err := os.ReadFile(fileName)
	if err != nil {
		log.Fatal(err)
	}
	// Count the characters
	characterCount := utf8.RuneCount(data)
	printOutput(characterCount, fileName, isFileFlag)
}

// printOutput
//
//	@Description: Print the output based on text or pipe function
//	@param count
//	@param fileName
//	@param isFileFlag
func printOutput(count interface{}, fileName string, isFileFlag bool) {
	if isFileFlag {
		fmt.Printf("%d   %s\n", count, fileName)
	} else {
		fmt.Printf("%d\n", count)
	}
}
